def generate_RH22_sensor(pin):
    import Adafruit_DHT

    def acquire():
        return Adafruit_DHT.read_retry(Adafruit_DHT.DHT22, str(pin))[::-1]

    return acquire


def generate_fake_sensor():
    import random

    def acquire():
        return random.gauss(30, 5), random.gauss(50, 10)

    return acquire
