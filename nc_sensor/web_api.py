#!/usr/bin/env python3

import pendulum

import bottle
from bottle import request, response
from bottle import post, get, put, delete

from loguru import logger
from nc_sensor.store import MongoStore

import pandas as pd

app = application = bottle.default_app()

store = MongoStore("chambre_nc")


def get_record(*, query=None, n=None):
    logger.info("query: %s" % query)
    records = store.table.find(query)
    if n:
        records = records.limit(2 * n)
    df = pd.DataFrame.from_records(records)
    if not len(df):
        return pd.DataFrame(columns=["temperature", "humidity"])
    df = df.drop("_id", axis=1).pivot(columns="sensor", index="date")
    df.columns = df.columns.droplevel()
    return df


def json_header():
    response.headers["Content-Type"] = "application/json"
    response.headers["Cache-Control"] = "no-cache"


@app.get("/by_date/<gt>/<lt>")
def get_between(gt, lt, parse=True):
    json_header()
    if parse:
        gt = pendulum.parse(gt)
        lt = pendulum.parse(lt)
    query = {"date": {"$gt": gt, "$lt": lt}}
    df = get_record(query=query)
    return df.to_json(date_format="iso")


@app.get("/by_date/<gt>")
def get_gt(gt, parse=True):
    json_header()
    if parse:
        gt = pendulum.parse(gt)
    query = {"date": {"$gt": gt}}
    df = get_record(query=query)
    return df.to_json(date_format="iso")


@app.get("/all")
def get_all_data():
    json_header()
    df = get_record()
    return df.to_json(date_format="iso")


@app.get("/last")
def get_last_entry():
    return get_last_n_entry(1)


@app.get("/last/index/<n:int>")
def get_last_n_entry(n):
    json_header()
    df = get_record(n=n)
    return df.to_json(date_format="iso")


@app.get("/last/<unit>/<value:float>")
def get_last_duration(unit, value):
    return get_gt(pendulum.now() - pendulum.duration(**{unit: value}), parse=False)


@app.get("/")
def get_last_day():
    return get_last_duration("days", 1)


def main():
    bottle.run(host="0.0.0.0", port=8000, reloader=True)


if __name__ == "__main__":
    main()
