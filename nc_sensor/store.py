import attr
import pymongo
from loguru import logger

@attr.s
class MongoStore:
    sensor_name = attr.ib(type=str)
    db_name = attr.ib(type=str, default="nc_sensors")
    client = attr.ib(type=pymongo.MongoClient)

    @client.default
    def _local_server(self):
        return pymongo.MongoClient("localhost")

    @property
    def db(self):
        return self.client[self.db_name]

    @property
    def table(self):
        return self.db[self.sensor_name]

    def save(self, data):
        for line in data:
            self.table.insert_one(line)
        logger.info("Lines inserted on mongo store.")

    def clean(self):
        self.table.drop()