#!/usr/bin/env python3

from apscheduler.schedulers.blocking import BlockingScheduler
import datetime
import attr
import pymongo
from functools import partial
import click
from loguru import logger
from nc_sensor.store import MongoStore

def acquire_data(acquire_function):
    now = datetime.datetime.now()
    temperature, humidity = acquire_function()
    logger.info("Data acquired: T = {temp:g} °C, HR = {hr:g} %"
                .format(temp=temperature, hr=humidity))
    for name, data in zip(("temperature", "humidity"), (temperature, humidity)):
        yield {"date": now, "sensor": name, "value": data}


def writer(acquire, store):
    store.save(acquire_data(acquire))


@click.command()
@click.option("--debug", is_flag=True)
@click.option("--clean", is_flag=True)
@click.option("--interval", default=10, type=float)
@click.option("--pin", default="4", type=str)
def main(debug, clean, interval, pin):

    if debug:
        from nc_sensor.sensor import generate_fake_sensor as generate_sensor
        acquire = generate_sensor()
    else:
        from nc_sensor.sensor import generate_RH22_sensor as generate_sensor
        acquire = generate_sensor(pin)

    
    store = MongoStore("chambre_nc")
    if clean:
        store.clean()
        return

    writer(acquire, store)

    scheduler = BlockingScheduler()
    scheduler.add_job(partial(writer, acquire, store), "interval", seconds=interval)
    scheduler.start()


if __name__ == '__main__':
    main()