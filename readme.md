# Simple sensor acquisition and rest API in python

The aim is to quickly setup a humidity and temperature sensor with low-cost
material, and make the data available on local network (or online).

## Hardware : DHT22 sensor on raspbery PI GPIO

The DHT22 is a coupled temperature and humidity sensor. It's a cheap sensor
(~8€), and you can replace it by a DHT11 which is less expensive (5 for ~10€).
The later can acquire less data per second, and has a narrower acquisition
range (0-50 °C instead of 0-80°C).

See [this tutorial](https://tutorials-raspberrypi.com/raspberry-pi-measure-humidity-temperature-dht11-dht22/)
for the hardware part.

## Software : data acquisition, storage and availability

Once the library installed (with `sudo pip3 install .`) you will have access to
two command line: `ncs_acquire --pin sensorpin` that will launch the sensor
acquisition and storage on mongodb, and `ncs_api` that will launch a local web
server that will expose the data. A [mongodb](https://www.mongodb.com/) instance
have to be running. You may have to modify the source if you aren't running
mongodb on the default port or if you want to use a different sensor.

You can also try the library without any sensor with `ncs_acquire --debug`.
Fake data are then generated.

Both command accept the `--help` option to see the different usage.

***This is a didactic package, do not hesitate to dive into the source.***

### Data processing

Once some data have been acquired, you can easily access it via

[http://my_raspberry_ip:8000/](http://my_raspberry_ip:8000/). The default root
give you access to the last 24 hours. You have also

- `/all`: all the data
- `/last`: the last entry
- `/last/index/<n:int>`: the lasts n entry
- `/last/<unit>/<value:float>`: all the entry more recent that \<value> \<unit>
  (2 days or 5 minutes for example).
- `/by_date/<gt>`: all entry after \<gt> (\<gt> being a date).
- `/by_date/<gt>/<lt>`: all entry between \<gt> and \<lt> (\<gt> and \<lt> being date).

For processing and plotting, you can easily retrieve the data via `pandas`.

```python
from scipy import stats

def remove_outlier(serie, threshold=3):
    z = np.abs(stats.zscore(serie))
    return serie.loc[z < threshold]

df = pd.read_json("http://192.168.1.13:8000/all")  # read all the data
remove_outlier(df.temperature).resample("1min").mean().plot(, marker=".")
```

## The brick of the software explained

### Data acquisition : AdaFruit DHT library

The library we will use is the the [Adafruit one](https://github.com/adafruit/Adafruit_Python_DHT).

Install it via pip:

```bash
sudo pip3 install Adafruit_DHT
```

You can now read the data from your sensor with

```python
while True:
    temperature, humidity = Adafruit_DHT.read_retry(Adafruit_DHT.DHT22, "4") # depend of where you have connected the sensor
    print("T: %g °C, HR: %g %" % (temperature, humidity))
```

**WARNING** You have to run the script as root, or the lib will not be able to
read the GPIO.

### Data storage : MongoDB

To save the data, `MongoDB` via the `pymongo` wrapper has been choosed. It is
more robust than simple (rotating) files and allow to quickly retrieve range
of data. As no-csv database, it suits well our very simple data.

You can simply install and run it it on your rapsbian with

```bash
sudo apt-get install mongodb
sudo systemctl enable mongodb  # enable run on startup
sudo systemctl start mongodb  # run it now
```

```python

client = pymongo.MongoClient("localhost")
db = client["my_database"]
sensor_table = db["sensors"]

now = datetime.datetime.now()
temperature, humidity = Adafruit_DHT.read_retry(Adafruit_DHT.DHT22, "4") # depend of where you have connected the sensor
sensor_table.insert_one({"date": now, "sensor": "temperature", "value": temperature})
sensor_table.insert_one({"date": now, "sensor": "humidity", "value": humidity})
```

### Data availability: REST API via the micro-framework bottle

There is many micro-framework. Two main exist for simple needs (without async
or part written in c for high performance) : Flask and bottle. I chose the
later, as being less powerful but more straight-forward. His templating is
also more straight-forward (being a superset of the python language).

```python
import bottle
import pymongo
from bottle import response
import pandas as pd

app = application = bottle.default_app()

client = pymongo.MongoClient("localhost")
db = client["my_database"]
sensor_table = db["sensors"]

@app.get("/all")
def get_all_data():
    response.headers["Content-Type"] = "application/json"  # the data will be in json
    response.headers["Cache-Control"] = "no-cache"  # as the data change often, avoid browser caching
    df = sensor_table.find()
    df = pd.DataFrame.from_records(records)
    # pivot the table to have cols ["date", "temperature", "humidity"]
    # instead of ["date", "sensor_name", "sensor_value"]
    df = df.drop("_id", axis=1).pivot(columns="sensor", index="date")
    df.columns = df.columns.droplevel()
    return df.to_json(date_format="iso")

def main():
    bottle.run(host="0.0.0.0", port=8000)

if __name__ == "__main__":
    main()
```

Going to [http://my_raspberry_ip:8000/all](http://my_raspberry_ip:8000/all),
you be able to access to all the data registered in the mongodb.

## TODO

- bokeh real-time plotting
- pre-processing (aggregate) via the web-API